# User Notification System

## Project Scenario
We are a Geospatial Insurtech company providing near real-time insights and reports to our clients on major global catastrophes. In this scenario, our system lacks a robust notification system that alerts users about events that matter to them.

You are tasked with leading a team to strategise and outline the development plan for implementing a User Notification System within our platform, this system should support email alerts and in-product notifications. 

Take time to understand our company and our field of work by consulting all public sources of data available to you.

## Project Requirements

1. **Proposal**
   - Develop a comprehensive proposal detailing the scope, objectives, deliverables, and timeline for implementing the User Notification System.
   - Outline how the notification system will enhance user engagement and provide value to our clients.
   - Identify key stakeholders and describe how their needs will be addressed.
   - Define the key performance indicatiors to measure the success of the system. 

2. **User and Market Research**
   - Conduct research to understand user needs and preferences regarding notifications.
   - Analyse competitive products and identify best practices to be incorporated into our system.

3. **Feature Prioritisation and Roadmap**
   - Identify and prioritise key features of the User Notification System.
   - Develop a product roadmap that includes a phased rollout plan.

4. **Collaboration Plan**
   - Outline a plan for cross-functional collaboration, detailing how you will work with engineering, design, marketing, and other teams.
   - Identify potential risks and mitigation strategies for successful collaboration.

5. **System Design and User Experience**
   - Provide a high-level overview of the system design that can be presented to non-technical stakeholders.
   - Describe the user experience, focusing on how notifications will be delivered, managed, and interacted with by the users.

6. **Documentation**
   - Prepare comprehensive documentation explaining the design rationale, development approach, and considerations for future enhancements.
   - Include wireframes or mockups to illustrate key aspects of the user interface.

## Submission Guidelines
- Submit your proposal, research findings, feature prioritisation, collaboration plan, system design overview, and detailed documentation in a cohesive format, such as a PDF or a presentation.

## Assessment Criteria
- Alignment of the proposal with project objectives and feasible timelines.
- Quality of user and market research, including insights and how they inform the project.
- Clarity and coherence of the feature prioritisation and product roadmap.
- Effectiveness of the collaboration plan, focusing on risk management and team alignment.
- Clear and user-focused system design and user experience considerations.
- Comprehensive and informative documentation addressing key aspects of the project.